/*!
 * @file    debug.cpp
 * @brief   整个项目的单元测试文件
 * @author  limou3434
 * @date    2024-05-11
 */

#include <iostream>
#include <string>
#include "verify/Debug.hpp"

 // 测试程序
int main(int argc, char* argv[]) {
    // // 初始化 GTest
    // ::testing::InitGoogleTest(&argc, argv);

    // // 运行所有测试用例
    // return RUN_ALL_TESTS();

    
}


// #include "tool/time.hpp"
// #include "tool/log.hpp"
// #include "tool/file.hpp"
// #include "tool/transfer.hpp"
// #include "data/config.hpp"
// #include "data/data_manager.hpp"
// using namespace CouldBackup;

// void MailDebug() {
//     CouldBackup::Message message("Test Subject", "This is a test email content.");	    // 创建邮件消息对象
//     CouldBackup::Mail mail("898738804@qq.com", "1346965749@qq.com");					// 创建邮件发送对象
//     mail.send(message);	                                                                // 发送邮件
// }

// void TimeDebug() {
//     std::cout << Time::TimeToStr(1716275818) << std::endl;
// }

// void LogDebug() {
//     Log log;
//     log.adjustment(false);
//     log.logMessage(DEBUG, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(INFO, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(WARNING, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(ERROR, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(FATAL, "This a message, %s %d.", __FILE__, __LINE__);
//     log.adjustment(true, ONE_FILE);
//     log.logMessage(DEBUG, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(INFO, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(WARNING, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(ERROR, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(FATAL, "This a message, %s %d.", __FILE__, __LINE__);
//     log.adjustment(true, CLASS_FILE);
//     log.logMessage(DEBUG, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(INFO, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(WARNING, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(ERROR, "This a message, %s %d.", __FILE__, __LINE__);
//     log.logMessage(FATAL, "This a message, %s %d.", __FILE__, __LINE__);
// }

// void TestCreateDirectory() {
//     std::cout << "创建目录文件\n";
//     File file("./path");
//     file.createDirectory();
//     std::cout << file.isDataFile() << std::endl;
// }
// void TestCreateDataFile() {
//     std::cout << "创建数据文件\n";
//     File file("./path/file.txt");
//     file.createDataFile(
//         R"(当夕阳余晖染红了天边的云彩，那一刻，大地仿佛沉浸在一片金色的梦境之中。微风轻拂，树叶在阳光下摇曳，发出轻柔的沙沙声，仿佛是自然演奏的乐章。
// 在这静谧的黄昏时刻，人们似乎感受到了一种来自内心深处的宁静与安详。或许是因为这一刻的美丽超越了言语的表达，唯有心灵的共鸣才能感知到这份深沉的情感。
// 在金色的余晖中，故事也在无声地展开。或许是一段爱情的故事，或许是一段别离的悲伤，又或许是一段对未来的憧憬和希望。无论是怎样的故事，都被这美丽的黄昏染上了一层温暖的色彩。
// 在这个瞬息万变的世界里，黄昏时刻似乎是一种稀缺的珍宝，它让人们停下脚步，静静地欣赏大自然的恩赐，感受生命的美好与意义。这一刻，人们仿佛找到了内心的平静和安宁，心灵也得到了净化和升华。
// 黄昏时刻的美丽，不仅在于它的外在景色，更在于它所带来的内心触动。在这一片金色的梦境中，人们或许会找到属于自己的一份宁静与美好。)"
// );

//     std::cout << file.isDirectory() << std::endl;
// }
// void TestGetFileSize() {
//     std::cout << "获取文件大小\n";
//     File file("./path/file.txt");
//     File dir("./path");
//     std::cout << "file " << file.getFileSize() << "\n";
//     std::cout << "dir " << dir.getFileSize() << "\n";
// }
// void TestGetLastModTime() {
//     std::cout << "获取文件的最后修改时间\n";
//     File file("./path/file.txt");
//     File dir("./path");
//     std::cout << Time::TimeToStr(file.getLastModTime()) << "\n";
//     std::cout << Time::TimeToStr(dir.getLastModTime()) << "\n";
// }
// void TestGetLastAccTime() {
//     std::cout << "获取文件的最后访问时间\n";
//     File file("./path/file.txt");
//     File dir("./path");
//     std::cout << Time::TimeToStr(file.getLastAccTime()) << "\n";
//     std::cout << Time::TimeToStr(dir.getLastAccTime()) << "\n";
// }
// void TestGetFileNameOfPath() {
//     std::cout << "获取文件路径中的文件名\n";
//     File file("./path/file.txt");
//     File dir("./path");
//     std::cout << file.getFileNameOfPath() << "\n";
//     std::cout << dir.getFileNameOfPath() << "\n";
// }
// void TestCompress() {
//     std::cout << "测试压缩和解压\n";
//     File file("./path/file.txt");
//     File zip("./path/file.txt.zip");
//     File copy("./path/file.txt.copy");

//     file.compress("./path/file.txt.zip");
//     std::cout << "压缩后大小: " << zip.getFileSize() << std::endl;
//     file.unCompress("./path/file.txt.copy");
//     std::cout << "解压后大小: " << copy.getFileSize() << std::endl;

//     // 检验解压后的文件在读取前后的访问时间和文件内容是否发生变化
//     std::string oldAccTime = Time::TimeToStr(copy.getLastAccTime());
//     sleep(10);
//     std::string body;
//     copy.getContent(&body);
//     std::string newAccTime = Time::TimeToStr(copy.getLastAccTime());
//     std::cout
//         << "文件解压后的内容: "
//         << body << '\n'
//         << "时间戳的变化: "
//         << oldAccTime << "-->" << newAccTime
//         << std::endl;
// }
// void TestRemove() {
//     std::cout << "删除数据文件和目录文件\n";
//     File file("./path/file.txt");
//     File dir("./path");
//     file.remove();
//     dir.remove();
// }
// void FileDebug() {
//     TestCreateDirectory(); // 测试创建目录文件
//     TestCreateDataFile(); // 测试创建数据文件
//     TestGetFileSize(); // 测试获取文件大小
//     TestGetLastModTime(); // 测试获取文件最后修改时间
//     TestGetLastAccTime(); // 测试获取文件最后访问时间
//     TestGetFileNameOfPath(); // 测试获取路径中的文件名
//     TestCompress(); // 测试压缩和解压方法
//     TestRemove(); // 测试删除文件
// }

// void TransferDebug() {
//     // 组织 JSON 数据
//     Json::Value root;
//     root["name"] = "Jane Doe";
//     root["age"] = 25;
//     root["is_student"] = true;

//     Json::Value address;
//     address["street"] = "456 Elm St";
//     address["city"] = "Othertown";
//     address["zipcode"] = "67890";
//     root["address"] = address;

//     Json::Value phoneNumbers(Json::arrayValue);
//     phoneNumbers.append("555-1234");
//     phoneNumbers.append("555-5678");
//     root["phone_numbers"] = phoneNumbers;

//     // 序列化
//     std::string jsonStr;
//     Transfer::JsonSerialize(root, &jsonStr);
//     std::cout << jsonStr << std::endl;

//     // 反序列化
//     Json::Value newVal;
//     Transfer::JsonUnSerialize(jsonStr, &newVal);
//     std::string name = newVal["name"].asString();
//     std::cout << name << std::endl;
//     std::cout << newVal << std::endl;
// }

// void ConfigDebug() {
//     Config* config = Config::getInstance();                 // 获取配置对象单例指针
//     std::cout << config->getServerIp() << std::endl;        // 获取服务器地址
//     std::cout << config->getServerPort() << std::endl;      // 获取服务器端口
//     std::cout << config->getHotTime() << std::endl;         // 获取热点时间
//     std::cout << config->getPackfileSuffix() << std::endl;  // 获取压缩后缀名
//     std::cout << config->getBackDir() << std::endl;         // 获取备份路径
//     std::cout << config->getPackDir() << std::endl;         // 获取压缩路径
//     std::cout << config->getBackUpFile() << std::endl;      // 获取持久化路径
//     std::cout << config->getDownloadPrefix() << std::endl;  // 获取请求路由
//     std::cout << config->getDatabaseName() << std::endl;    // 获取数据库名字
//     for (auto it : config->getTableNames()) {               // 获取数据库名字   
//         std::cout << "数据表: " << it << std::endl;               
//     }
// }

// void DataManagerDebug() {
//     // 创建文件和目录
//     File backDir("../asset/back_dir");
//     File packDir("../asset/pack_dir");
//     File file_1("../asset/back_dir/test_1.txt");
//     File file_2("../asset/back_dir/test_2.txt");
//     backDir.createDirectory();
//     packDir.createDirectory();
//     file_1.createDataFile("I am text_1.");
//     file_2.createDataFile("I am text_2.");

//     // 制作备份记录
//     BackUpInfo info_1(file_1);
//     BackUpInfo info_2(file_2);
//     std::cout << "制作备份记录: " << info_1 << "-" << info_2 << "\n";

//     // 管理备份记录
//     // DataManager* dm = DataManager::getInstance();
//     // dm->insert(info_1);
//     // dm->insert(info_2);
//     // std::vector<BackUpInfo> infos;
//     // dm->getAllBackInfo(&infos);
//     // for (auto info : infos) {
//     //     std::cout << info << std::endl;
//     // }
//     // std::cout << dm << std::endl;

//     // // 持久化记录
//     // dm->storge();

//     // // 读取备份记录文件内容
//     // std::string body;
//     // File(Config::getInstance()->getBackUpFile()).getContent(&body);
//     // std::cout
//     //     << "备份记录文件内容如下:\n"
//     //     << body << std::endl;
// }

// void Debug() {
//     // MailDebug();
//     // TimeDebug();
//     // LogDebug();
//     // FileDebug();
//     // TransferDebug();
//     // ConfigDebug();
//     // DataManagerDebug();
// }

// int main() {
//     Debug();
//     return 0;
// }