/*!
 * @file
 * @brief  整个项目的启动文件
 *
 * 如果希望阅读源代码, 可以从 main() 这里看起
 *
 * @author  limou3434
 * @date    2024-05-11
 */

/*!
 * \class  ClassName
 * \brief  Brief description of the class.
 *
 * Detailed description of the class if needed.
 */
#include "../include/app/app.hpp"
int main()
{
    CouldBackup::App app;
    app.Run();
    return 0;
}