/*!
 * @file
 * @brief  项目的启动逻辑管理
 *
 * 项目启动的运行逻辑中必备的函数
 *
 * @author  limou3434
 * @date    2024-05-11
 */

#pragma once
#include <string>
#include <sstream>
#include <functional>
#include "../data/configer.hpp"
#include "../data/data_manager.hpp"
#include "../vendor/http-lib/include/httplib.h"
#include "../tool/logger.hpp"
#include "../tool/filer.hpp"
#include "../tool/time.hpp"

//TODO-2024-5-3-ljp: 这里可以尝试设计为一个单例类, 考虑做成父类继承, 这样就不用每一个类都单独做成单例了...
namespace CouldBackup {
    class App {
    //内置函数
    private:
        //从请求路径中提取文件名    
        std::string GetETag(const BackUpInfo& info) {
            std::string etag = Filer(info._realPath).GetFileNameOfPath();
            etag += "-";
            etag += std::to_string(info._fileSize);
            etag += "-";
            etag += std::to_string(info._modTime);
            return etag;
        }


    //接口定义
    private:
        //文件上传接口
        void Upload(const httplib::Request& req, httplib::Response& res) {
            //接口调用测试
            _log->LogMessage(DEBUG, "/upload 接口被调用, %s %d", __FILE__, __LINE__);

            //检查表单中是否存在 "file" 字段, 进行文件备份
            auto ret = req.has_file("file");

            if (ret == false) { //服务端获取文件异常
                _log->LogMessage(WARNING, "Upload()->req.has_file() error, %s %d", __FILE__, __LINE__);
                res.status = 400;
            } else { //服务端获取文件成功
                //获取文件内容
                const auto& file = req.get_file_value("file");
                _log->LogMessage(
                    DEBUG, "从客户端处获取文件成功\n文件名字: %s\n文件内容: %s\n %s %d",
                    file.filename.c_str(),
                    file.content.c_str(),
                    __FILE__, __LINE__
                );

                //备份用户上传的文件
                std::string backRealPath = _config->GetBackDir() + "/" + file.filename; //得到原始文件的备份存储路径
                Filer afile = Filer(backRealPath);
                afile.CreateDataFile(file.content); //创建文件并且写入内容

                //创建对应的备份记录并且插入数据管理中
                BackUpInfo info(backRealPath);
                _dm.Insert(info);

                //备份信息做下持久化
                _dm.Storge();
                res.status = 200;

                //跳转回原来的界面
                Show(req, res);
            }
        }

        //文件查看接口
        void Show(const httplib::Request& req, httplib::Response& res) {
            //接口调用测试
            _log->LogMessage(DEBUG, "/show 接口被调用, %s %d", __FILE__, __LINE__);

            //获取所有文件的备份信息
            std::vector<BackUpInfo> infos;
            _dm.GetAll(&infos);

            //编写为 html 文档返回给用户
            std::stringstream htmlBody;
            htmlBody << R"(<html><head><title>云备份-备份列表</title></head><body><h1>已备份文件列表</h1><table>)";
            for (auto info : infos) { //TODO: 可以考虑做一个类似 Jinja2 的模板渲染 //TODO: 这里可以考虑补全备份记录的中文件的名字, 还有调用接口, 不要使用内部类成员
                htmlBody << R"(<tr>)";
                htmlBody << R"(<td><a href=")" << info._downloadPathOfUrl << R"(">)" << info._realPath << R"(</a></td>)";
                htmlBody << R"(<td align = "right">)" << Time::TimeToStr(info._modTime) << R"(</td>)"; //文件修改时间
                htmlBody << R"(<td align = "right">)" << info._fileSize << R"(KB</td>)";
                htmlBody << R"(</tr>)";
                
                _log->LogMessage(DEBUG, "%s %s %d %d, %s %d",
                    info._downloadPathOfUrl.c_str(),
                    info._realPath.c_str(),
                    info._modTime,
                    info._fileSize,
                    __FILE__, __LINE__
                );
            }
            htmlBody << R"(</table>)";
            htmlBody << R"(<form action="http://111.230.60.61:8081/upload" method="post" enctype="multipart/form-data">)";
            htmlBody << R"(<input type="file" name="file" id="file">)";
            htmlBody << R"(<input type="submit" value="提交文件">)";
            htmlBody << R"(</form>)";
            htmlBody << R"(</body>)";

            //设置文件内容
            res.body = htmlBody.str();
            res.set_header("Content-Type", "text/html; charset=utf-8");

            //设置返回状态码
            res.status = 200;
        }

        //文件下载接口
        void Download(const httplib::Request& req, httplib::Response& res) {
            //接口调用测试
            _log->LogMessage(DEBUG, "/download 接口被调用, %s %d", __FILE__, __LINE__);

            //根据请求资源路径, 寻找备份路径中对应的文件
            BackUpInfo info;
            _dm.GetOneByUrl(req.path, &info);

            //若文件被压缩, 则解压缩修改备份信息
            if (info._packFlag == true) { //是则解压, 修改内存数据库并做持久化
                Filer packFile(info._packPath);
                packFile.UnCompress(info._realPath);
                packFile.Remove();
                info._packFlag = false;
                _dm.UpData(info);
                _dm.Storge();
            }

            //判断请求中是否有断点续传的相关字段
            bool retrans = false;
            if (req.has_header("If-Range") == true) {
                std::string oldEtag = req.get_header_value("If-Range");
                if (oldEtag == GetETag(info)) { //说明断点续传过程中文件没有发生改变, 符合断点续传的要求
                    retrans = true;
                }
            }

            //没有 If-Range 字段或者该字段与当前文件的 etag 不一致(出现改动), 则都认为需要进行正常下载功能

            Filer backFile(info._realPath);

            //设置响应头部字段和状态
            if (retrans == false) {
                _log->LogMessage(DEBUG, "正常下载"); //TODO: 可以给日志函数重载一个简单版本的
                backFile.GetContent(&res.body); //获取文件的内容填入响应中等待发送
                res.set_header("Accept-Ranges", "bytes"); //设置 "Accept-Ranges" 标头的作用是告诉客户端服务器是否支持范围请求, 允许客户端请求资源的某个指定范围内的部分内容, 而不是整个资源, 对大型文件比较有用 "bytes" 表示服务器支持按字节范围请求的方式, 即客户端可以通过指定起始和结束字节位置来请求部分内容。
                res.set_header("ETag", GetETag(info)); //"Etag" 提供了一种机制来标识和验证资源的版本信息，以便于缓存控制、条件请求和资源验证, 这里是用于缓存控制
                // res.set_header("Content-Type", "text/html; charset=utf-8");
                // res.set_header("Content-Type", "text/markdown; charset=utf-8"); //markdown 文档
                res.set_header("Content-Type", "application/octet-stream"); //二进制数据流, 常用于文件下载
                res.status = 200;
            }
            else {
                _log->LogMessage(DEBUG, "断点续传"); //TODO: 可以给日志函数重载一个简单版本的
                backFile.GetContent(&res.body); //获取文件的内容填入响应中等待发送
                //std::string range = req.get_header_val("Rangr"); --> bytes=start-end
                res.set_header("Accept-Ranges", "bytes");
                res.set_header("ETag", GetETag(info));
                res.set_header("Content-Type", "application/octet-stream"); //二进制数据流, 常用于文件下载
                //res.set_header("Content-Range", "bytes start-end/fsize");
                res.status = 206; //区间请求, 这里其实可以不用设置, httplib 内部会自动设置
            }


            //断点续传的原理就是用户每次下载文件, 收到数据写入文件时, 记录自己当前下载的数据量, 下次恢复下载时, 请求重新下载的数据区间即可(下载起始位置, 结束位置)
            //还需要注意的是, 下次断点续传的时候, 需要判断该文件是否在服务器上被修改, 若有则停止断点续传, 重新从头开始下载
            //TODO: 思考其他的提高下载速度的思路
        }


    public:
        //构造
        App() {
            _config         = Configer::GetInstance();
            _serverPort     = _config->GetServerPort();
            _serverIp       = _config->GetServerIp();
            _downloadPrefix = _config->GetDownloadPrefix();
            _log            = Logger::GetInstance();

            _log->LogMessage(DEBUG, "%s:%d->%s, %s %d", _serverIp.c_str(), _serverPort, _downloadPrefix.c_str(), __FILE__, __LINE__);
        }
        
        //绑定资源接口、运行服务程序
        bool Run() {
            //绑定资源接口
            //TODO-2024-5-3-ljp: 这里本来我是希望使用 bind() 的但是最好发现没办法成功, 最后只能采用 lambda 表达式
            _server.Post("/upload", [this](const httplib::Request& req, httplib::Response& res) { Upload(req, res); });
            _server.Get("/", [this](const httplib::Request& req, httplib::Response& res) { Show(req, res); });
            _server.Get("/show", [this](const httplib::Request& req, httplib::Response& res) { Show(req, res); });
            _server.Get(std::string("/") + _downloadPrefix + "/(.*)", [this](const httplib::Request& req, httplib::Response& res) { Download(req, res); }); //. 是匹配除了 \n 和 \r 以外的单字符, * 则是使用前种子表达式匹配任意次

            //运行服务程序
            if (_server.listen(_serverIp, _serverPort) == false) {
                _log->LogMessage(FATAL, "Run()->_server.listen() error %s %d", __FILE__, __LINE__);
            }
        }


    private:
        Configer*           _config;
        int                 _serverPort;
        std::string         _serverIp;
        std::string         _downloadPrefix;
        httplib::Server     _server;
        Logger*             _log;
        DataManager         _dm;
    };
}