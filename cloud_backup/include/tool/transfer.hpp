/*!
 * @file
 * @brief	项目的序列解析工具
 * @author  limou3434
 * @date    2024-05-11
 */

#pragma once
#include <memory>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include "tool/log.hpp"
#include "json-cpp/include/json/json.h"

namespace CouldBackup {
    /* 管理传输序列的类 */
    class Transfer {
    private:
        // 序列化
        bool _JsonSerialize(const Json::Value& root, std::string* str) {
            Json::StreamWriterBuilder swb;
            std::unique_ptr<Json::StreamWriter> sw(swb.newStreamWriter());
            std::stringstream ss;

            if (sw->write(root, &ss) != 0) {
				_log.logMessage(WARNING, "序列化出错, %s %d", __FILE__, __LINE__);
                return false;
            }

            *str = ss.str(); //从字符串流中读取整个字符串
            return true;
        }

        // 反序列化
        bool _JsonUnSerialize(const std::string& str, Json::Value* root) {
            Json::CharReaderBuilder crb;
            std::unique_ptr<Json::CharReader> cr(crb.newCharReader());
            std::string err;
            bool ret = cr->parse(str.c_str(), str.c_str() + str.size(), root, &err);

            if (ret == false) {
                _log.logMessage(WARNING, "反序列化出错, %s %d", __FILE__, __LINE__);
                return false;
            }

            return true;
        }

    public:
        static bool JsonSerialize(const Json::Value& root, std::string* str) {
            Transfer t;
            return t._JsonSerialize(root, str);
        }

        static bool JsonUnSerialize(const std::string& str, Json::Value* root) {
            Transfer t;
            return t._JsonUnSerialize(str, root);
        }

    private:
        Log _log;
    }; // class Jsoner end
} // namespace CouldBackup end