/*!
 * @file
 * @brief   项目的时间转化工具
 * @author  limou3434
 * @date    2024-05-11
 */

#pragma once
#include <sstream>
#include <ctime>

namespace CouldBackup {
    /*!
     * @brief   提供将时间戳转换为字符串的工具函数
     */
    class Time {
    public:
        /*!
         * @brief   将时间戳转换为字符串表示形式
         * @param   timestamp 时间戳
         * @return  时间戳的字符串表示形式
         */
        static std::string TimeToStr(const std::time_t& timestamp) {
            std::string str = std::ctime(&timestamp);
            std::size_t pos = str.rfind('\n');
            if (pos != std::string::npos) {
                str.erase(pos);
            }
            return str;
        }
    };
}
