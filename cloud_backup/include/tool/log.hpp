/*!
 * @file
 * @brief	项目的日志打印工具
 * @author  limou3434
 * @date    2024-05-11
 */

#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <mutex>
#include <cstdio>
#include <cstdarg>
#include <ctime>
#include <pthread.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

namespace CouldBackup {
    // 日志级等级
    const int DEBUG = 0;    // 调试
    const int INFO = 1;     // 正常
    const int WARNING = 2;  // 警告
    const int ERROR = 3;    // 错误
    const int FATAL = 4;    // 致命

    // 日志打印模式
    enum WriteMode {
        SCREEN = 5,
        ONE_FILE,
        CLASS_FILE
    };

    /*!
	 * @brief   管理日志的类
	 */
    class Log {
    public:
        // 日志等级和日志字符的映射数组
        // TODO: 有时间研究一下, 看看这里的数组无法使用类内 const 变量的原因
        const char* gLevelMap[5] = {
            "debug",    //debug 模式
            "info",   //正常
            "warning",  //警告
            "error",    //非致命错误
            "fatal"     //严重错误
        };
        const std::string logdir = "log_dir"; //日志文件名

        
    private:
        // 日志信息统一写入文件
        void _writeLogToOneFile(std::string logFileName, const std::string& message) const {
            std::ofstream out(logFileName, std::ios::app);
            if (!out.is_open())
                return;
            out << message;
            out.close();
        }

        // 日志信息分类写入文件
        void _writeLogToClassFile(const int& level, const std::string& message) const {
            std::string logFileName = "./";
            logFileName += logdir;
            logFileName += "/";
            logFileName += _logFileName;
            logFileName += "_";
            logFileName += gLevelMap[level];

            _writeLogToOneFile(logFileName, message);
        }

        // 按照不同模式输出日志
        void _writeLog (const int& level, const std::string& message) const {
            switch (_writeMode) {
            case SCREEN: //向屏幕输出
                std::cout << message;
                break;
            case ONE_FILE: //向单个日志文件输出
                _writeLogToOneFile("./" + logdir + "/" + _logFileName, message);
                break;
            case CLASS_FILE: //向多个日志文件输出
                _writeLogToClassFile(level, message);
                break;
            default:
                std::cout << "write mode error!!!" << std::endl;
                break;
            }
        }

        
    public:
        // 构造函数, debugShow 为是否显示 debug 消息, writeMode 为日志打印模式, logFileName 为日志文件名
        Log(bool debugShow = true, const WriteMode& writeMode = SCREEN, std::string logFileName = "log")
            : _debugShow(debugShow)
            , _writeMode(writeMode)
            , _logFileName(logFileName) {
            mkdir(logdir.c_str(), 0775); // 创建目录
        }

        // TODO: 有时间研究一下, 这里如果有静态成员, 使用 default 的赋值重载貌似无效
        // 拷贝构造, 允许拷贝传递
        Log(const Log& other) = default;

        // 赋值重载, 允许赋值(注意类的静态成员没有被重载)
        Log& operator=(const Log& other) { // TODO: 这点值得补充学习, 赋值重载的语义就导致 *this 对象无法被赋值, 因此就不应该成为 const 函数
            if (this != &other) {
                this->_debugShow = other._debugShow;
                this->_writeMode = other._writeMode;
                this->_logFileName = other._logFileName; 
            }
            return *this;
        }

        // 调整日志打印方式
        void adjustment(bool debugShow = true, const WriteMode& writeMode = SCREEN, std::string logFileName = "log") {
            //申请智能锁, 防止多线程情况下, 在日志对象修改属性期间输出日志信息
            std::lock_guard<std::mutex> lock(_mtx);
            _debugShow = debugShow;
            _writeMode = writeMode;
            _logFileName = logFileName;
        }

        // 拼接日志消息并且输出, level = DEBUG | INFO | WARNING | ERROR | FATAL
        void logMessage (const int& level, const char* format, ...) const {
            // 申请智能锁, 防止多线程情况下, 日志消息混乱
            std::lock_guard<std::mutex> lock(_mtx);

            // 1.若不是 debug 模式, 且 level == DEBUG 则不做任何事情
            if (_debugShow == false && level == DEBUG)
                return;

            // 2.收集日志标准部分信息
            char stdBuffer[1024];
            time_t timestamp = time(nullptr); //获得时间戳
            struct tm* local_time = localtime(&timestamp); //将时间戳转换为本地时间

            snprintf(stdBuffer, sizeof stdBuffer, "[%s][pid:%s][%d-%d-%d %d:%d:%d]",
                gLevelMap[level],
                std::to_string(getpid()).c_str(),
                local_time->tm_year + 1900, local_time->tm_mon + 1, local_time->tm_mday,
                local_time->tm_hour, local_time->tm_min, local_time->tm_sec
            );

            // 3.收集日志自定义部分信息
            char logBuffer[1024];
            va_list args; //声明可变参数列表, 实际时一个 char* 类型
            va_start(args, format); //初始化可变参数列表
            vsnprintf(logBuffer, sizeof logBuffer, format, args); //int vsnprintf(char *str, size_t size, const char *format, va_list ap); 是一个可变参数函数, 将格式化后的字符串输出到缓冲区中。类似带 v 开头的可变参数函数有很多
            va_end(args); //清理可变参数列表, 类似 close() 和 delete

            // 4.拼接为一个完整的消息
            std::string message;
            message += "--> 标准日志:"; message += stdBuffer;
            message += "\t 用户日志:"; message += logBuffer;
            message += "\n";

            // 5.打印日志消息
            _writeLog(level, message);
        }

        
private:
        bool            _debugShow;     //> 是否显示 DEBUG 的信息
        WriteMode       _writeMode;     //> 日志显示模式
        std::string     _logFileName;   //> 日志文件名称
        static std::mutex      _mtx;    //> 全局日志互斥锁
    }; // class Log end
    std::mutex Log::_mtx; // 静态成员定义, 相当于全局类域内的锁, 让项目的所有日志输出都原子化
} // namespace CouldBackup end
