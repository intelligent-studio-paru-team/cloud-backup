/*!
 * @file
 * @brief	项目的文件操作工具
 * @author  limou3434
 * @date    2024-05-11
 */

#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <experimental/filesystem> 
#include <cstdio>
#include <ctime>
#include <sys/stat.h>
#include <utime.h>
#include "tool/log.hpp"
#include "data/data_manager.hpp"
#include "bundle/include/bundle.h"

namespace CouldBackup {
	namespace fs = std::experimental::filesystem; //简化命名空间

	class File {
	private:
		using file_stat_t = struct stat;


	private:
		// 获取文件的所有属性
		file_stat_t _getFileStar(void) const {
			//1.尝试获取文件的所有属性
			file_stat_t st;
			int result = stat(_filePath.c_str(), &st);

			//2.检查是否获取成功
			if (result < 0) {
				_log.logMessage(WARNING, "获取文件属性失败, %s %d", __FILE__, __LINE__);
			}

			return st;
		}


	public:
		// 构造文件管理对象, 传入文件路径
		File(const std::string& filePath)
			: _filePath(filePath), _PATH_SEP('/')
		{}

		// 创建目录文件
		bool createDirectory(void) {
			if (exists() == true) { // 如果文件已经存在, 则无法创建
				_log.logMessage(WARNING, "目录文件已经存在无法重复创建, %s %d", __FILE__, __LINE__);
				return false;
			}
			else {
				fs::create_directory(_filePath);
				return true;
			}
		}

		// 创建数据文件
		bool createDataFile(const std::string& body = "") {
			if (exists() == true) { // 如果文件已经存在, 则无法创建
				_log.logMessage(WARNING, "数据文件已经存在无法重复创建, %s %d", __FILE__, __LINE__);
				return false;
			}

			if (setContent(body) == false) { // 写一个空内容就可以创建出文件, 而如果用户传递了文件的内容, 可以进对文件的内容进行初始化
				_log.logMessage(WARNING, "空数据文件写入数据出错, %s %d", __FILE__, __LINE__);
				return false;
			}

			return true;
		}

		// 获取文件的大小
		std::int64_t getFileSize(void) const {
			return _getFileStar().st_size;
		}

		// 获取文件最后一次修改时间
		time_t getLastModTime(void) const {
			return _getFileStar().st_mtime;
		}

		// 获取文件最后一次访问时间
		time_t getLastAccTime(void) const {
			return _getFileStar().st_atime;
		}

		// 获取文件的真实路径
		std::string getFileRealPath() const {
			return _filePath;
		}

		// 获取路径中的文件名
		std::string getFileNameOfPath(void) const {
			// 1.从末尾开始寻找分隔符
			std::size_t pos = _filePath.rfind(_PATH_SEP);

			// 2.检查是否找到
			if (pos == std::string::npos) {
				return _filePath;
			}

			// 3.返回文件名
			return _filePath.substr(pos + 1);
		}

		// 带出数据文件指定位置、指定长度的内容(同时更新访问时间, 修改内存中的备份记录文件)
		bool getPosLenOfContent(std::string* body, const std::size_t& pos, const size_t& len) const {
			// 如果连文件都不存在就别走下去了
			if (exists() == false) {
				_log.logMessage(WARNING, "获取内容的文件不存在, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 连数据文件都不是就别走下去了
			if (isDataFile() == false) {
				_log.logMessage(WARNING, "获取内容的文件不是数据文件, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 检查 pos+len 的合法性
			if (pos + len > getFileSize()) {
				_log.logMessage(WARNING, "获取数据的范围有误, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 尝试打开文件
			std::ifstream ifs;
			ifs.open(_filePath, std::ios::binary);

			// 检查是否成功打开文件
			if (ifs.is_open() == false) {
				_log.logMessage(WARNING, "打开文件失败, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 读取文件中 pos+len 的数据
			ifs.seekg(pos, std::ios::beg); // 相对于开头跳转到偏移位置
			body->resize(len); // 提前调整 body 的大小
			ifs.read(&((*body)[0]), len); // 读取文件内容带回给用户

			// 检查是否读取成功
			if (ifs.good() == false) { // 检查是否处于良好状态
				_log.logMessage(WARNING, "读取内容的过程中发生错误, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 关闭文件
			ifs.close();

			// 更新访问时间
			updateActime();

			// 更新内存中的备份记录文件
			DataManager::getInstance()->updata(BackUpInfo(getFileRealPath())); // 主要是更新备份记录中的最后访问时间
			return true;
		}

		// 带出数据文件所有内容(同时更新访问时间, 修改内存中的备份记录文件)
		bool getContent(std::string* body) {
			return getPosLenOfContent(body, 0, getFileSize());
		}

		// 带出遍历目录文件下的子文件
		bool getDirectoryArray(std::vector<std::string>* fileArry) {
			// 如果连文件都不存在就别走下去了
			if (exists() == false) {
				_log.logMessage(WARNING, "文件不存在, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 连目录文件都不是就别走下去了
			if (isDirectory() == false) {
				_log.logMessage(WARNING, "无法遍历目录文件, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 遍历每一个子文件, 设 B 是 A 的子文件, 那对于 A 来说, B 的相对路径就是 ./A/B
			for (auto& p : fs::directory_iterator(_filePath)) { // 使用目录迭代器
				fileArry->push_back(fs::path(p).relative_path().string()); // push 进相对路径
			}

			return true;
		}

		// 判断文件是否存在
		bool exists(void) const {
			return fs::exists(_filePath);
		}

		// 判断文件是否为目录文件
		bool isDirectory() {
			fs::path filePath = _filePath.c_str();
			return fs::is_directory(filePath);
		}

		// 判断文件是否为数据文件
		bool isDataFile() const {
			fs::path filePath = _filePath.c_str();
			return fs::is_regular_file(filePath);
		}

		// TODO: 但是暂时不支持父目录递归创建
		// 设置数据文件内容(不存在则创建, 存在则覆盖且自动更改访问时间, 修改内存中的备份记录文件)
		bool setContent(const std::string& newBody) {
			// 尝试打开文件
			std::ofstream ofs;
			ofs.open(_filePath, std::ios::binary); //以二进制形式代开数据文件, 如果数据文件不存在则自动创建

			// 检查是否成功打开文件
			if (ofs.is_open() == false) {
				_log.logMessage(WARNING, "创建并打开文件过程出错, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 写内容到文件中
			ofs.write(newBody.c_str(), newBody.size());

			// 检查是否成功写入
			if (ofs.good() == false) {
				_log.logMessage(WARNING, "写入文件出错, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 关闭文件
			ofs.close();

			// 由于修改文件的时候操作系统自动更新修改时间, 这里就直接更新内存中的备份记录即可
			DataManager::getInstance()->updata(BackUpInfo(getFileRealPath())); // 主要是更新备份记录中的最后修改时间
			return true;
		}

		// 压缩数据文件
		bool compress(const std::string& packName) {
			// 如果连文件都不存在就别走下去了
			if (exists() == false) {
				_log.logMessage(WARNING, "文件不存在, %s %d", __FILE__, __LINE__);
				return false;
			}

			if (isDataFile() == false) { // 连数据文件都不是就别走下去了
				_log.logMessage(WARNING, "不是数据文件, 请选用其他压缩接口, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 1.获取源数据
			std::string body;
			if (getContent(&body) == false) {
				_log.logMessage(WARNING, "获取文件失败, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 2.进行数据压缩
			std::string packed = bundle::pack(bundle::LZIP, body);

			// 3.存储压缩文件
			File fu(packName);
			fu.setContent(packed);

			return true;
		}

		// 解压数据文件
		bool unCompress(const std::string& unPackName) {
			// 如果连文件都不存在就别走下去了
			if (exists() == false) {
				_log.logMessage(WARNING, "压缩包文件不存在, %s %d", __FILE__, __LINE__);
				return false;
			}

			if (isDataFile() == false) { // 连数据文件都不是就别走下去了
				_log.logMessage(WARNING, "这不是一个数据文件无法解压, %s %d", __FILE__, __LINE__);
				return false;
			}

			// 1.获取源数据
			std::string body;
			if (getContent(&body) == false) {
				_log.logMessage(WARNING, "获取压缩文件内容失败 %s %d", __FILE__, __LINE__);
				return false;
			}

			// 2.对数据进行解压
			std::string unpacked = bundle::unpack(body);

			// 3.存储解压文件
			File fu(unPackName);
			fu.setContent(unpacked);
			return true;
		}

		// 删除文件
		bool remove(void) {
			if (exists() == false) { // 如果文件不存在, 不予以删除
				_log.logMessage(WARNING, "文件不存在无需删除, %s %d", __FILE__, __LINE__);
				return false;
			}

			fs::remove_all(_filePath.c_str()); //目录和文件都可以删除

			return true;
		}

		// 更新文件访问时间戳
		bool updateActime() const {
			// 设置文件新的状态
			struct utimbuf new_times;
			new_times.actime = time(nullptr); // 修改访问时间为当前时间戳
			new_times.modtime = _getFileStar().st_mtime; // 保留修改时间戳

			// 更新文件的状态
			if (utime(_filePath.c_str(), &new_times) != 0) {
				return false;
			}

			return true;
		}

	private:
		std::string _filePath; 	//> 被管理文件的真实路径
		const char 	_PATH_SEP; 	//> 设置文件分隔符
		Log			_log; 		//> 日志对象
	}; //class File end
} //namespace CouldBackup end