/*!
 * @file
 * @author limou
 * @date 2024-05-21
 */

#pragma once
#include <iostream>
#include <string>
#include <utility>
#include <Poco/Net/MailMessage.h>
#include <Poco/Net/SMTPClientSession.h>
#include <Poco/Net/SecureSMTPClientSession.h>
#include <Poco/Net/NetException.h>
#include "verify/Message.hpp"

namespace verify {
	/*!
	* @brief 邮箱类
	*
	* 内部封装关于邮箱的发送者和接收者, 以及对应的邮箱服务地址、邮箱授权码、邮箱服务端口号, 默认使用 SMTP
	*/
	class Mail {
	public:
		/*!
		 * @brief 构造函数, 创建一个邮箱管理对象
		 * @param[in] sender        发件人邮箱
		 * @param[in] recipient     收件人邮箱
		 * @param[in] smtpServer    SMTP 服务器地址
		 * @param[in] smtpCode      SMTP 服务器授权码
		 * @param[in] smtpPort      SMTP 服务器端口
		 */
		Mail(
			const std::string& sender,
			const std::string& recipient,
			const std::string& smtpServer, // 例如 "smtp.qq.com"
			const std::string& smtpCode, // 例如 std::getenv("SMTP_CODE")
			const int& smtpPort // 例如 25
		)
			: _sender(sender)
			, _recipient(recipient)
			, _smtpServer(smtpServer)
			, _smtpCode(smtpCode)
			, _smtpPort(smtpPort)
		{}


	public:
		/*!
		 * @brief 发送邮件
		 * @param[in] subject 邮件主题
		 * @param[in] content 邮件内容
		 */
		void send(const Message& message) const {
			Poco::Net::MailMessage mess;
			// 设置双方邮箱
			mess.setSender(_sender);
			mess.addRecipient(Poco::Net::MailRecipient(Poco::Net::MailRecipient::PRIMARY_RECIPIENT, _recipient));

			// 设置邮件内容
			mess.setSubject(message.getSubject());
			mess.setContent(message.getContent());

			// 完成发送邮件
			Poco::Net::SecureSMTPClientSession smtp(_smtpServer, _smtpPort);
			smtp.login(Poco::Net::SMTPClientSession::LoginMethod::AUTH_LOGIN, _sender, _smtpCode); // 登录 SMTP 服务器
			smtp.sendMessage(mess);
			smtp.close();
		}
	

		/*!
		 * @brief 修改发件人信息
		 * @param[in] sender 新的发件人邮箱
		 * @param[in] smtpServer 新的 SMTP 服务器地址
		 * @param[in] smtpCode 新的 SMTP 服务器授权
		 * @param[in] smtpCode 新的 SMTP 服务器端口
		 */
		void setSenderInfo(
			const std::string& sender,
			const std::string& smtpServer,
			const std::string& smtpCode,
			const int& smtpPort
		) {
			_sender = sender;
			_smtpServer = smtpServer;
			_smtpCode = smtpCode;
		}

		/*!
		 * @brief 修改收件人信息
		 * @param[in] recipient 新的收件人邮箱
		 */
		void setRecipientInfo(const std::string& recipient) { _recipient = recipient; }


	private:
		std::string 	_sender; 		//< 发件人邮箱
		std::string 	_recipient; 	//< 收件人邮箱
		std::string 	_smtpServer;	//< SMTP 服务器地址
		std::string 	_smtpCode;	 	//< SMTP 服务器授权
		int 			_smtpPort;	 	//< SMTP 服务器端口
	}; // class Mail end
} // namespace verify end
