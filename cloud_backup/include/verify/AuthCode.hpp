/*!
 * @file
 * @author limou
 * @date 2024-05-21
 */

#pragma once
#include <random>
#include <chrono>
#include <string>
#include "verify/Mail.hpp"
#include "verify/Message.hpp"

namespace verify {
    /*!
    * @brief 验证码类
    *
    * 是继承自 Mail 类的子类
    */
    class AuthCode : public Mail {
    private:
        /*!
		 * @brief 查看当前验证码是否过期
         * @param[in] timeLimit 验证码过期期限
         * @return 返回布尔值, true 为过期
         */
        bool isExpired(const intmax_t& timeLimit) const {
            // _oldTimestamp 尚未被赋值, 直接当作过期处理
            if (_oldTimestamp == std::chrono::time_point<std::chrono::system_clock>::min()) {
                return true; // 已经过期
            }

            // _oldTimestamp 已经被更新, 需要根据时间差值做进一步的判断
            auto newTimestamp = std::chrono::system_clock::now(); // 获取当前的时间戳
            auto duration = newTimestamp - _oldTimestamp; // 计算新旧时间间隔
            auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration).count(); // 将时间间隔转换为秒

            // 如果时间间隔超过验证码存活期限则说明过期
            if (seconds >= timeLimit) {
                return true; // 已经过期
            } else {
                return false; // 尚未过期
            }
        }

        /*!
		 * @brief 获取 codeLen 位验证码
         * @param[in] codeLen   需要得到的验证码长度
         * @param[in] timeLimit 验证码过期期限
         * @return 返回一串最新的验证码
         */
        std::string getRandomCode(const int& codeLen, const intmax_t& timeLimit) {
            // 如果检查验证码没有过期, 直接返回之前存储的验证码即可
            if (isExpired(timeLimit) == false) {
                return _oldRandomCode;
            }

            // 验证码过期, 重新使用随机数生成器生成验证码
            std::random_device rd; // 提供高质量随机种子的对象
            std::mt19937 gen(rd()); // 使用种子初始伪随机生成器
            std::uniform_int_distribution<int> dis(0, 9); // 随机数分布对象, 可以生成指定范围 [0, 9] 内的随机数

            // 构建随机验证码字符串
            std::string newRandomCode;
            for (int i = 0; i < codeLen; ++i) {
                newRandomCode += std::to_string(dis(gen));
            }

            // 返回验证码之前, 先把这次生成的验证码存储起来, 并且重新设置时间戳
            _oldRandomCode = newRandomCode;
            _oldTimestamp = std::chrono::system_clock::now();

            return _oldRandomCode;
        }

    public:
        /*!
		 * @brief 构造, 使用发送者和接收者来创建一个验证码管理对象
         * @param[in] sender    发送者
         * @param[in] recipient 接收者
         */
        AuthCode(
            const std::string& sender,
            const std::string& recipient
        )
            : Mail(sender, recipient, "smtp.qq.com", std::getenv("SMTP_CODE"), 25)
            , _oldTimestamp(std::chrono::time_point<std::chrono::system_clock>::min())
            , _oldRandomCode("")
        {}

        /*!
		 * @brief 重写父类 Mail 的发送接口, 只发送对应的验证码消息, 会自动检验验证码是否过期并且进行更新, 使用者只需要进行发送即可
         * @param[in] codeLen   需要得到的验证码长度, 默认值为 6 位长度
         * @param[in] timeLimit 验证码过期期限, 单位为秒, 默认值为 60s 内
         * @return 返回一串最新的验证码
         */
        void send(const int& codeLen = 6, const intmax_t& timeLimit = 60) {
            std::string code = getRandomCode(codeLen, timeLimit); // 获取存活时间为 timeLimit 且长度为 codeLen 验证码
            Message m = Message("云备份平台验证码", "验证码为: " + code); // 整理要发送端的消息
            Mail::send(m); // 使用父类方法发送验证码
        }

    private:
        std::chrono::time_point<std::chrono::system_clock>   _oldTimestamp;     //> 旧的一份时间戳
        std::string                                          _oldRandomCode;    //> 旧的一份验证码
    }; // class AuthCode end
} // namespace verify end
