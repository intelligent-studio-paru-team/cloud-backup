/*!
 * @file
 * @author  limou
 * @date    2024-05-21
 */

#include <iostream>
#include <unistd.h>
#include <gtest/gtest.h>
#include "verify/AuthCode.hpp"

// 定义测试用例
TEST(VerifyTest, MessageTest_1) {
    std::string head = "一个标题";
    std::string body = "一段正文";
    verify::Message mess = verify::Message();
    mess.setSubject(head);
    mess.setContent(body);
    EXPECT_EQ(head, mess.getSubject()); // 相等就返回 true
    EXPECT_EQ(body, mess.getContent()); // 相等就返回 true
}

TEST(VerifyTest, MessageTest_2) {
    verify::Message mess = verify::Message("一个标题", "一段正文");
    EXPECT_EQ("一个标题", mess.getSubject()); // 相等就返回 true
    EXPECT_EQ("一段正文", mess.getContent()); // 相等就返回 true
}

TEST(VerifyTest, MailTest) {
    verify::Message mess = verify::Message("一个标题", "一段正文");
    verify::Mail m = verify::Mail("898738804@qq.com", "1346965749@qq.com", "smtp.qq.com", std::getenv("SMTP_CODE"), 25);
    m.send(mess);
    GTEST_LOG_(INFO) << "发送邮件成功";
}

TEST(VerifyTest, AuthCode) {
    verify::AuthCode a("898738804@qq.com", "1346965749@qq.com");
    // TODO: 可以考虑测试验证码是否更新, 但是有些耗时, 可以尝试使用多执行流优化这一次测试
    // int count = 5;
    // while (count--) {
    //     a.send();
    //     GTEST_LOG_(INFO) << "发送一次验证码成功";
    //     sleep(60);
    // }
    a.send();
    GTEST_LOG_(INFO) << "发送验证码成功";
}