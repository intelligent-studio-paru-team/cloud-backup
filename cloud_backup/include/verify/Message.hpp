/*!
 * @file
 * @author limou
 * @date 2024-05-21
 */

#pragma once
#include <string>

namespace verify {
    /*!
     * @brief 消息类
     *
     * 封装一个消息类, 内部包含主题和内容
     */
    class Message {
    public:
        /*!
         * @brief 构造, 创建一个消息对象
         * @param[in] subject 主题, 默认值为 None subject
         * @param[in] content 内容, 默认值为 None content
         */
        Message(
            const std::string& subject = "None subject",
            const std::string& content = "None content"
        ) : _subject(subject), _content(content) {}


    public:
        /*!
         * @brief 设置消息主题
         * @param[in] subject 消息主题
         */
        void setSubject(const std::string& subject) { _subject = subject; }

        /*!
         * @brief 获取消息主题
         * @return 返回消息主题
         */
        std::string getSubject() const { return _subject; }

        /*!
         * @brief 设置消息内容
         * @param[in] content 消息内容
         */
        void setContent(const std::string& content) { _content = content; }

        /*!
         * @brief 获取消息内容
         * @return 返回消息内容
         */
        std::string getContent() const { return _content; }

    private:
        std::string _subject; //< 邮件主题
        std::string _content; //< 邮件内容
    }; // class Message end
} // namespace verify end