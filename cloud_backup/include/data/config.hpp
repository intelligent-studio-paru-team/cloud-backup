/*!
 * @file
 * @brief   项目的相关配置管理
 * @author  limou3434
 * @date    2024-05-11
 */

#pragma once
#include <mutex>
#include <string>
#include <vector>
#include "tool/log.hpp"
#include "tool/file.hpp"
#include "tool/transfer.hpp"

namespace CouldBackup {
    /*!
	* @brief   配置类, 读取整个项目的配置
	*/
    class Config
    {
    private:
        // 读取配置文件并且做配置
        bool _setConfig(const std::string& configFilePath) {
            // 读取配置文件内容
            File f(configFilePath);
            std::string body;
            if (f.getContent(&body) == false) {
				_log.logMessage(WARNING, "读取配置文件内容失败, %s %d", __FILE__, __LINE__);
                return false;
            }

            // 反序列化文件内容
            Json::Value root;
            if (Transfer::JsonUnSerialize(body, &root) == false) {
				_log.logMessage(WARNING, "_setConfig()->UnSerialize() error, %s %d", __FILE__, __LINE__);
                return false;
            }

            // 初始化各项配置
            _serverIp       = root["serverIp"].asString();
            _serverPort     = root["serverPort"].asInt();
            _hotTime        = root["hotTime"].asInt();
            _packfileSuffix = root["packfileSuffix"].asString();
            _backDir        = root["backDir"].asString();
            _packDir        = root["packDir"].asString();
            _backUpFile     = root["backUpFile"].asString();
            _downloadPrefix = root["downloadPrefix"].asString();
            _databaseName   = root["databaseName"].asString();
            _tableNames = {
                 root["tablesNames"]["table1"].asString(), 
                 root["tablesNames"]["table2"].asString()
            };
            return true;
        }

        // 私有构造
        Config(const std::string& confugFilePath) {
            _setConfig(confugFilePath); //读取 .json 内的配置
        }

        // TODO: 关于单例指针析构函数是否需要处理, 这里待讨论

    public:
        // 禁用拷贝构造
        Config(const Config& self) = delete;

        // 禁用赋值重载
        Config& operator=(const Config& self) = delete;
        
        // 获取到单例对象
        static Config* getInstance(const std::string& confugFilePath = "../config/app_config.json") {
            if (_instance == nullptr) { // 如果之前没有获取过, 就进行单例获取
                std::lock_guard<std::mutex> lock(_mutex); // 智能锁加锁

                if (_instance == nullptr) {
                    _instance = new Config(confugFilePath); // 懒汉模式
                } // end if
            }

            return _instance;
        }

        // 获取服务器地址
        std::string getServerIp(void) { return _serverIp; }

        // 获取服务器端口
        int getServerPort(void) { return _serverPort; }

        // 获取到热点时间
        int getHotTime(void) { return _hotTime; }

        // 获取压缩后缀名
        std::string getPackfileSuffix(void) { return _packfileSuffix; }

        // 获取备份的路径
        std::string getBackDir(void) { return _backDir; }

        // 获取压缩的路径
        std::string getPackDir(void) { return _packDir; }

        // 获取备份记录文件路径
        std::string getBackUpFile(void) { return _backUpFile; }

        // 获取请求的前缀
        std::string getDownloadPrefix(void) { return _downloadPrefix; }

        // 获取数据库名称
        std::string getDatabaseName(void) { return _databaseName; }

        // 获取数据表名称列表
        std::vector<std::string> getTableNames(void) { return _tableNames; }

        
    private:
        Log                         _log;               // 日志对象
        static std::mutex           _mutex;             // 互斥锁
        static Config*              _instance;          // 懒汉单例指针
        std::string                 _serverIp;          // 服务运行地址
        int                         _serverPort;        // 服务监听端口
        int                         _hotTime;           // 热点判定时间
        std::string                 _packfileSuffix;    // 压缩包名后缀
        std::string                 _backDir;           // 备份文件存储路径
        std::string                 _packDir;           // 压缩文件存储路径
        std::string                 _backUpFile;        // 备份信息文件路径
        std::string                 _downloadPrefix;    // 下载时 URL 中 Path 的前缀
        std::string                 _databaseName;      // 项目数据库
        std::vector<std::string>    _tableNames;        // 项目数据表
    }; // class Config end

    Config* Config::_instance = nullptr;
    std::mutex Config::_mutex;
} // namespace CouldBackup end
