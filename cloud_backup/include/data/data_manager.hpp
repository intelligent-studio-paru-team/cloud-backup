/*!
 * @file
 * @brief   项目的数据配置管理
 * @author  limou3434
 * @date    2024-05-11
 */

 /* 说明文档
 BackUpInfo info; info.MakeNewBackUpInfo("realPath");        // 构造单个备份记录
 或
 BackUpInfo info("realPath");

 std::cout << info;                                          // 打印设置好的一条备份记录

 DataManager dm;                                             // 构造管理备份记录的对象, 内部自动 _initLoad() 进行旧备份记录的加载

 dm.Insert(info);                                            // 插入备份记录
 dm.Storge();                                                // 做持久化操作

 BackUpInfo oldInfo
 dm.getOneByUrl("downloadPathOfUrl", &oldInfo);   // 根据资源下载路径进行查找(如果插入的数据没有持久化, 也可以被查询到)
 或
 dm.getOneByRealPath("realPath", &oldInfo);       // 根据文件真实路径进行查找(如果插入的数据没有持久化, 也可以被查询到)

 std::vector<BackUpInfo> array;  dm.getAllBackInfo(&array);          // 查询所有的备份记录(如果插入的数据没有持久化, 也可以被查询到)

 oldInfo.attribute = newValue;                               // 修改备份记录
 dm.UpData(oldInfo);                                         / /修改修复记录
 dm.Storge();                                                // 再次持久化操作

 std::cout << dm;                                            // 打印所有的备份记录
 */

#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <pthread.h>
#include "tool/file.hpp"
#include "tool/time.hpp"
#include "tool/transfer.hpp"
#include "tool/log.hpp"
#include "data/config.hpp"

namespace CouldBackup {
    /* 备份记录类 */
    class BackUpInfo {
    public:
        // 构造空的备份记录对象
        BackUpInfo() {}

        // 构造, 通过文件的真实路径制作文件的备份记录对象
        BackUpInfo(const std::string& realPath) {
            if (makeNewBackUpInfo(realPath) == false) {
                return;
            }
        }

        // 构造, 通过 File 对象构造备份记录
        BackUpInfo(const File& file) {
            if(makeNewBackUpInfo(file.getFileRealPath()) == false) {
                return;
            }
        }

        // 拷贝构造, 使用默认的即可
        BackUpInfo(const BackUpInfo& other) = default;

        // 赋值重载, 使用默认的即可
        BackUpInfo& operator=(const BackUpInfo& other) = default;

        
    public:
        // 制作一个新的文件备份信息
        bool makeNewBackUpInfo(const std::string& realPath) {
            File f(realPath);

            if (f.exists() == false) {
                _log.logMessage(WARNING, "找不到该文件无法创建备份记录, %s %d", __FILE__, __LINE__);
                return false;
            }

            Config* config = Config::getInstance();
            std::string packDir = config->getPackDir(); // 获取配置文件中的压缩路径
            std::string packFileSuffix = config->getPackfileSuffix(); // 获取配置文件中的压缩后缀
            std::string downloadPrefix = config->getDownloadPrefix(); // 获取配置文件中的下载接口前缀

            _packFlag = false;
            _fileSize = f.getFileSize();
            _modTime = f.getLastModTime();
            _accTime = f.getLastAccTime();
            _realPath = realPath;
            _packPath = packDir + "/" + f.getFileNameOfPath() + packFileSuffix;
            _downloadPathOfUrl = "/" + downloadPrefix + "/" + f.getFileNameOfPath();

            return true;
        }

        // 获取压缩标志位
        bool getPackFlag() const { return _packFlag; }
        void setPackFlag(bool packFlag) { _packFlag = packFlag; }

        // 获取文件大小
        std::size_t getFileSize() const { return _fileSize; }
        void setFileSize(const std::size_t& fileSize) { _fileSize = fileSize; }

        // 获取最后一次修改时间
        std::time_t getModTime() const { return _modTime; }
        void setModTime(const std::time_t& modTime) { _modTime = modTime; }

        // 获取最后一次访问时间
        std::time_t getAccTime() const { return _accTime; }
        void setAccTime(const std::time_t& accTime) { _accTime = accTime; }

        // 获取原始文件实际存储路径
        std::string getRealPath() const { return _realPath; }
        void setRealPath(const std::string& realPath) { _realPath = realPath; }

        // 获取压缩文件实际存储路径
        std::string getPackPath() const { return _packPath; }
        void setPackPath(const std::string& packPath) { _packPath = packPath; }

        // 获取资源请求路径
        std::string getDownloadPathOfUrl() const { return _downloadPathOfUrl; }
        void setDownloadPathOfUrl(const std::string& downloadPathOfUrl) { _downloadPathOfUrl = downloadPathOfUrl; }

    private:
        bool            _packFlag;          // 压缩标志位
        std::size_t     _fileSize;          // 文件的大小
        std::time_t     _modTime;           // 最后一次修改时间
        std::time_t     _accTime;           // 最后一次访问时间
        std::string     _realPath;          // 原始文件实际存储路径 "./back_dir/test.txt"
        std::string     _packPath;          // 压缩文件实际存储路劲 "./pack_dir/test.txt.lz"
        std::string     _downloadPathOfUrl; // 资源请求路径
        Log             _log;               // 日志对象
    };

    // 流重载, 打印一条备份记录的所有信息
    std::ostream& operator<<(std::ostream& os, const BackUpInfo& info) {
        os
            << "a back info -->\n"
            << "target:\t" << info.getPackFlag() << '\n'
            << "size:\t" << info.getFileSize() << '\n'
            << "modtime:\t" << Time::TimeToStr(info.getModTime()) << '\n'
            << "acctime:\t" << Time::TimeToStr(info.getAccTime()) << '\n'
            << "real path:\t" << info.getRealPath() << '\n'
            << "pack path:\t" << info.getPackPath() << '\n'
            << "download path:\t" << info.getDownloadPathOfUrl() << '\n'
            << "-----------------\n";
        return os;
    }


    /* 数据持久化类, 管理所有备份信息(一旦被创建就会自动加载内存中的备份数据库), 相当于小型可持久化的内存数据库 */
    class DataManager {
        friend std::ostream& operator<<(std::ostream& os, const DataManager& dm);


    private:
        // 加载旧的备份记录数据到 _table 中(内存中)
        bool _initLoad(void) {
            // 1.把之前的备份好备份记录读取回来
            File f(Config::getInstance()->getBackUpFile());
            if (f.exists() == false) {
                _log.logMessage(DEBUG, "之前没有备份过, 无备份记录可加载, %s %d", __FILE__, __LINE__);
                return false;
            }

            std::string body;
            if (f.getContent(&body) == false) {
                _log.logMessage(WARNING, "读取备份记录文件失败, %s %d", __FILE__, __LINE__);
                return false;
            }

            // 2.反序列化
            Json::Value root;
            if (Transfer::JsonUnSerialize(body, &root) == false) {
                _log.logMessage(WARNING, "反序列化备份记录文件失败, %s %d", __FILE__, __LINE__);
                return false;
            }

            // 3.将反序列化内容加载到 _table 中, 类似于内存数据库进行刷盘
            for (auto aJson : root) {
                BackUpInfo info;
                info.setPackFlag(aJson["packFlag"].asBool());
                info.setFileSize(aJson["fileSize"].asUInt64());
                info.setModTime(aJson["modTime"].asUInt64());
                info.setAccTime(aJson["accTime"].asUInt64());
                info.setRealPath(aJson["realPath"].asString());
                info.setPackPath(aJson["packPath"].asString());
                info.setDownloadPathOfUrl(aJson["downloadUrl"].asString());
                insert(info);
            }

            return true;
        }

        // 创建备份目录和压缩目录
        bool _createBackdirAndPackdir(void) {
            Config* config = Config::getInstance();

            File backDir = File(config->getBackDir());
            File packDir = File(config->getPackDir());

            if (backDir.exists() == false) {
                _log.logMessage(DEBUG, "备份目录不存在, 可能是第一次启动项目或者目录丢失, 现在将重新进行创建, %s %d", __FILE__, __LINE__);
                backDir.createDirectory();
            }

            if (packDir.exists() == false) {
                _log.logMessage(DEBUG, "压缩目录不存在, 可能是第一次启动项目或者目录丢失, 现在将重新进行创建, %s %d", __FILE__, __LINE__);
                packDir.createDirectory();
            }
        }

        // 判断是否为热点文件
        bool _isHot(const File& file) {
            // 如果超出热点时间则不是热点文件
            time_t hotTime = Config::getInstance()->getHotTime();   // 获取热点时间判定标准
            time_t curTime = time(nullptr);                         // 获取当前系统的时间戳
            time_t lastTime = file.getLastAccTime();                // 获取当前文件的最后一次访问时间戳
            if (curTime - lastTime > hotTime) { // 如果时间间隔大于规定的热点时间就是非热点文件
                _log.logMessage(DEBUG, "找到一个可以被压缩的非热点文件, %s %d", __FILE__, __LINE__);
                return false;
            }

            return true; // 是热点文件
        }
        
        // 私有构造, 主要是加载旧的备份记录, 检查备份和压缩目录是否需要建立
        DataManager() {
            // 初始化读写锁-读共享/写互斥
            pthread_rwlock_init(&_rwlock, NULL);

            // 加载之前的备份记录
            _initLoad();

            // 创建备份目录和压缩目录
            Config* config = Config::getInstance();
            File backDir = File(config->getBackDir());
            File packDir = File(config->getPackDir());
            if (backDir.exists() == false) {
                _log.logMessage(DEBUG, "备份目录不存在, 可能是第一次启动项目或者目录丢失, 现在将重新进行创建, %s %d", __FILE__, __LINE__);
                backDir.createDirectory();
            }
            if (packDir.exists() == false) {
                _log.logMessage(DEBUG, "压缩目录不存在, 可能是第一次启动项目或者目录丢失, 现在将重新进行创建, %s %d", __FILE__, __LINE__);
                packDir.createDirectory();
            }
        }

        // 私有析构
        ~DataManager() {
            pthread_rwlock_destroy(&_rwlock); // 销毁读写锁-读共享/写互斥
            storge(); // 退出时持久化一下, 把内存中的备份记录信息存储到磁盘上(类似内存转储)
            // TODO: 关于单例指针析构函数是否需要处理, 这里待讨论
        }

        
    public:
        // 禁用拷贝构造
        DataManager(const DataManager& self) = delete;

        // 禁用赋值重载
        DataManager& operator=(const DataManager& self) = delete;
        
        // 获取到数据管理单例对象
        static DataManager* getInstance(const std::string& confugFilePath = "../config/app_config.json") {
            if (_instance == nullptr) { // 如果之前没有获取过, 就进行单例获取
                std::lock_guard<std::mutex> lock(_mutex); // 智能锁加锁

                if (_instance == nullptr) {
                    _instance = new DataManager(); // 懒汉模式
                } // end if
            }

            return _instance;
        }
        
        // 在内存中新增一条备份记录, 键值对以资源下载路径为 key, 备份记录为 value
        void insert(const BackUpInfo& info) {
            _log.logMessage(DEBUG, "新增一条记录, %s %d", __FILE__, __LINE__);
            pthread_rwlock_wrlock(&_rwlock); // 添加写锁
            _table[info.getDownloadPathOfUrl()] = info;
            pthread_rwlock_unlock(&_rwlock); // 释放写锁
        }

        // 在内存中修改一条备份记录, 这里的实现和新增数据实际类似
        void updata(const BackUpInfo& info) {
            _log.logMessage(DEBUG, "更新一条记录, %s %d", __FILE__, __LINE__);
            pthread_rwlock_wrlock(&_rwlock); // 添加写锁
            _table[info.getDownloadPathOfUrl()] = info;
            pthread_rwlock_unlock(&_rwlock); // 释放写锁
        }

        // 在内存中删除一条备份记录
        void remove(const BackUpInfo& info) {
            _log.logMessage(DEBUG, "删除一条记录, %s %d", __FILE__, __LINE__);
            pthread_rwlock_wrlock(&_rwlock); // 添加写锁
            _table.erase(info.getDownloadPathOfUrl());
            pthread_rwlock_unlock(&_rwlock); // 释放写锁
        }

        // 根据资源请求路径查询, 并从内存中带出对应的单条备份信息
        bool getOneByUrl(const std::string& downloadUrl, BackUpInfo* info) {
            pthread_rwlock_rdlock(&_rwlock); // 添加读锁

            auto it = _table.find(downloadUrl);
            if (it == _table.end()) { // 没有查询到请求资源路径对应的备份记录
                pthread_rwlock_unlock(&_rwlock); // 释放读锁
                _log.logMessage(DEBUG, "没有找到对应的备份记录, %s %d", __FILE__, __LINE__);
                return false;
            } else { // 查询到请求资源路径对应的备份记录, 带出备份记录
                *info = it->second;
                pthread_rwlock_unlock(&_rwlock); // 释放读锁
                return true;
            }
        }

        // 根据文件真实路径查询, 并从内存中带出对应的单条备份信息
        bool getOneByRealPath(const std::string& realPath, BackUpInfo* info) {
            pthread_rwlock_rdlock(&_rwlock); // 添加读锁

            for (const auto& it : _table) { // 遍历整个表, 查询真实路径和表中相同的
                if (it.second.getRealPath() == realPath) { // 查询到
                    *info = it.second;
                    pthread_rwlock_unlock(&_rwlock); // 释放读锁
                    return true;
                }
            }

            _log.logMessage(DEBUG, "没有, %s %d", __FILE__, __LINE__);
            return false; //没有查询到
        }

        // 获取所有的备份信息, 并从内存中以备份记录数组带出
        bool getAllBackInfo(std::vector<BackUpInfo>* array) {
            pthread_rwlock_rdlock(&_rwlock); //添加读锁
            for (auto it : _table) {
                array->push_back(it.second);
            }
            pthread_rwlock_unlock(&_rwlock); //释放读锁
            return true;
        }

        // 持久化操作
        bool storge() {
            // 1.以备份记录数组的形式获取所有的备份信息
            std::vector<BackUpInfo> infoArray;
            if (getAllBackInfo(&infoArray) == false) {
                _log.logMessage(WARNING, "获取所有备份记录失败, %s %d", __FILE__, __LINE__);
                return false;
            }

            // 2.添加备份信息属性到 JSON 文件中
            Json::Value root;
            for (const BackUpInfo& info : infoArray) {
                Json::Value item;
                item["packFlag"]    = info.getPackFlag();
                item["fileSize"]    = static_cast<Json::UInt64>(info.getFileSize());
                item["modTime"]     = static_cast<Json::UInt64>(info.getModTime());
                item["accTime"]     = static_cast<Json::UInt64>(info.getAccTime());
                item["realPath"]    = info.getRealPath();
                item["packPath"]    = info.getPackPath();
                item["downloadUrl"] = info.getDownloadPathOfUrl();

                root.append(item);
            }

            // 3.序列化
            std::string body;
            if (Transfer::JsonSerialize(root, &body) == false) {
                _log.logMessage(WARNING, "备份记录序列化失败, %s %d", __FILE__, __LINE__);
                return false;
            }

            // 4.写入文件
            File backUpFile = File(Config::getInstance()->getBackUpFile());
            if (backUpFile.setContent(body) == false) {
                _log.logMessage(WARNING, "备份记录的 JSON 文件写入失败, %s %d", __FILE__, __LINE__);
                return false;
            }

            return true;
        }

        // 备份信息热点管理, 扫描备份文件列表和遍历文件的属性, 根据时间戳差值决定是否需要压缩
        void hotspot(void) {
            // 获取备份目录下的所有文件, 注意是真实存在的文件, 而不是读取内存中已有的(避免有文件缺漏备份, 一种防御对策)
            File backDir(Config::getInstance()->getBackDir());
            std::vector<std::string> fileNames;
            if (backDir.getDirectoryArray(&fileNames) == false) {
                _log.logMessage(WARNING, "获取备份文件下的文件失败, %s %d", __FILE__, __LINE__);
            }

            // 遍历每一个备份文件的名字
            for (const auto& fileName : fileNames) {
                File file(fileName);

                // TODO: 有可能用户需要存储目录文件, 这种清空比较复杂, 等待迭代

                // 是目录文件则重新循环
                if (file.isDirectory() == true) {
                    _log.logMessage(DEBUG, "检测遇到目录文件, %s %d", __FILE__, __LINE__);
                    continue;
                }

                // 是热点文件则重新循环
                if (_isHot(file) == true) {
                    _log.logMessage(DEBUG, "检测遇到热点文件, %s %d", __FILE__, __LINE__);
                    continue;
                }

                _log.logMessage(DEBUG, "检测遇到非热点文件, 其对应的路径为 %s, 尝试进行压缩... %s %d", fileName.c_str(), __FILE__, __LINE__);

                // 获取非热点文件的备份信息
                DataManager dm;
                BackUpInfo info;

                // 这里可能出现一个问题, 文件存在, 但是之前没有备份成功(防御措施)
                if (dm.getOneByRealPath(fileName, &info) == false) {
                    info.makeNewBackUpInfo(fileName); // 那就只能新建立一个备份记录
                    _log.logMessage(WARNING, "文件存在但是没有备份记录, 因此增加一份记录, %s %d", __FILE__, __LINE__);
                }

                // 对非热点文件进行压缩, 并且删除源文件
                _log.logMessage(DEBUG, "%s 开始对非热点文件进行压缩... %s %d", fileName.c_str(), __FILE__, __LINE__);
                info.setPackFlag(true); // 设置压缩标志位
                file.compress(info.getPackPath()); // 开始压缩
                file.remove(); // 删除源文件

                // 更新备份信息并做持久化
                dm.updata(info);
                dm.storge();
            } // for end
        }


    private:
        Log                                         _log;       // 日志对象
        static std::mutex                           _mutex;     // 互斥锁
        static DataManager*                         _instance;  // 懒汉单例指针
        std::unordered_map<std::string, BackUpInfo> _table;     // 相当于内存中的数据库, <K-V> 对即 <资源下载路径-备份记录>
        pthread_rwlock_t                            _rwlock;    // 读写锁-读共享/写互斥(读写锁中, 要么一个线程在写, 要么多个线程在读, 两种情况不会同时出现)
    }; // class DataManager end

    DataManager* DataManager::_instance = nullptr;
    std::mutex DataManager::_mutex;

    // 流重载, 遍历打印备份记录列表
    std::ostream& operator<<(std::ostream& os, const DataManager& dm) {
        std::cout << "The backinfos ------------------>>\n";
        for (const auto& it : dm._table) {
            std::cout << it.second << std::endl;
        }
        std::cout << "----------------------------------\n";
        return os;
    }
} // namespace CouldBackup end